FROM axonasif/gitpod-workspace-full-vnc:latest

USER gitpod

RUN sudo apt-get update && \
    sudo apt-get install -y python3-pyqt5 openssh-server
RUN sudo install-packages mongodb-server && \
    sudo mkdir -p /data/db_mongo && \
    sudo chown gitpod:gitpod -R /data/db_mongo
RUN sudo install-packages mysql-server \
 && sudo mkdir -p /var/run/mysqld /var/log/mysql \
 && sudo chown -R gitpod:gitpod /etc/mysql /var/run/mysqld /var/log/mysql /var/lib/mysql /var/lib/mysql-files /var/lib/mysql-keyring /var/lib/mysql-upgrade
 

# Install our own MySQL config
COPY mysql_docker_data/mysql.cnf /etc/mysql/mysql.conf.d/mysqld.cnf

# Install default-login for MySQL clients
COPY mysql_docker_data/client.cnf /etc/mysql/mysql.conf.d/client.cnf

COPY mysql_docker_data/mysql-bashrc-launch.sh /etc/mysql/mysql-bashrc-launch.sh

RUN sudo chmod +x /etc/mysql/mysql-bashrc-launch.sh && \
    echo "/etc/mysql/mysql-bashrc-launch.sh" >> /home/gitpod/.bashrc.d/100-mysql-launch
