'''main file for workshop demo application'''
import argparse  # command line argument parses
import configparser  # read/write config files in ini format
import pathlib  # for file path operations
import json  # read/write json data
import pickle  # read/write serialized binary data
import random  # for random samples
# from pip
import openpyxl # read Excel files
import yaml  # read/write yml data
import bson  # read/write serialized binary json data


class Config():
    '''Base config class that holds filename and default values.'''
    def __init__(self, name):
        self.name = name
        self.default = {'DEFAULT':
                       {'db_path': '../db_mysql',
                        'initialized': 'false',
                        'last_init_file': '',
                        'data_format': 'pickle'}}

    def create_default_config(self):
        ''' Create default config file.'''
        with open(self.name, 'w', encoding='utf-8') as conf_file:
            self.write(conf_file)

    def read_config(self):
        ''' Read from default config file.'''
        with open(self.name, 'r', encoding='utf-8') as conf_file:
            config = self.read(conf_file)
        return config


class IniConfig(Config):
    ''' Config class for ini files.'''
    def write(self, conf_file):
        ''' Create default ini config content.'''
        config = configparser.ConfigParser()
        config['DEFAULT'] = self.default['DEFAULT']
        config.write(conf_file)

    def read(self, conf_file):
        ''' Read existing ini config.'''
        config = configparser.ConfigParser()
        config.read_file(conf_file)
        return config


class JsonConfig(Config):
    ''' Config class for json files.'''
    def write(self, conf_file):
        ''' Create default json config content.'''
        json.dump(self.default, conf_file, sort_keys=True, indent=4)

    def read(self, conf_file):
        ''' Read existing json config.'''
        return json.load(conf_file)


class YmlConfig(Config):
    ''' Config class for yml files.'''
    def write(self, conf_file):
        ''' Create default yml config content.'''
        yaml.dump(self.default, conf_file)

    def read(self, conf_file):
        ''' Read existing yml config.'''
        return yaml.safe_load(conf_file)

def read_xlsx(file_name):
    ''' Read data from an xlsx and return as list-of-dicts.'''
    workbook = openpyxl.load_workbook(file_name, read_only=True)
    worksheet = workbook.active  # get sheet
    headers = []

    for column in range(1, worksheet.max_column+1):
        headers.append(worksheet.cell(row=1, column=column).value)

    data = []
    for row in range(2, worksheet.max_row+1):
        item = {}
        for column in range(1, worksheet.max_column+1):
            item[headers[column-1]] = worksheet.cell(row=row, column=column).value
        if item['Tool'] is not None:
            data.append(item)

    workbook.close()
    return data


def main(args):
    ''' Main function for control flow. Takes pre-process argument object.'''

    # todo:
    # check for and read existing ini config file
    configs = [IniConfig('config.ini'),
               JsonConfig('config.json'),
               YmlConfig('config.yml')]

    for conf_obj in configs:
        if pathlib.Path(conf_obj.name).is_file():
            config = conf_obj.read_config()
        # if notexistent, create with default values
        else:
            conf_obj.create_default_config()
            config = conf_obj.read_config()

        # print(config['DEFAULT']['initialized'])

    # if given as parameter, read in xlsx file
    if args.init:
        print(f"Trying to import data from {args.init}")
        data = read_xlsx(args.init)
    # else, read in from pickle file
    elif config['DEFAULT']['data_format'] == 'pickle' and \
         pathlib.Path('data.pickle').is_file():
        with open('data.pickle', 'br') as data_file:
            data = pickle.load(data_file)
    elif config['DEFAULT']['data_format'] == 'bson' and \
         pathlib.Path('data.bson').is_file():
        with open('data.bson', 'br') as data_file:
            data_dict = bson.loads(data_file.read())
            data = data_dict['DATA']  # remove outer dict

    if args.dump_examples and data:
        for i in random.sample(range(0, len(data)), args.dump_examples):
            print(data[i])

    # save data (without modification)
    if config['DEFAULT']['data_format'] == 'pickle':
        with open('data.pickle', 'bw') as data_file:
            pickle.dump(data, data_file)
    elif config['DEFAULT']['data_format'] == 'bson':
        with open('data.bson', 'bw') as data_file:
            # extra-enclose in outer dict
            data_file.write(bson.dumps({'DATA':data}))


    # for -d, print n random samples to the console
    # for -v, print a version number

if __name__ == "__main__":
    # Main entry point. Pre-parses command line arguments.
    parser = argparse.ArgumentParser(
        description='Inventory handling with xls import')
    parser.add_argument(
        '-d', '--dump_examples', metavar='n', type=int,
        help='print n samples to the console')
    parser.add_argument(
        '-i', '--init', metavar='filename',
        help='init the database with entries from this xlsx file')
    parser.add_argument(
        '-v', '--version', action='version', version='%(prog)s 0.1',
        help='print version number')
    args_dict = parser.parse_args()

    # call the main function with the pre-processed arguments
    main(args_dict)
