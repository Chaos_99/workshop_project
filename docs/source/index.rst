.. Workshop Inventory Manager documentation master file, created by
   sphinx-quickstart on Tue Apr  5 12:20:34 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Workshop Inventory Manager's documentation!
======================================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

.. autosummary::

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
